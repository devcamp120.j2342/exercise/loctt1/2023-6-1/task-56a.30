package com.devcamp.s10.task56a30.restapi;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class EmployeeController {
     @GetMapping("/employees")
     public List<Employee> getEmployees(){
          new ArrayList<>();  

          //thực hiện khởi tạo 3 đối tượng sinh viên
          Employee employee1 = new Employee(1, "Loc", "Tan", 10);
          Employee employee2 = new Employee(2, "Cam", "My", 20);
          Employee employee3 = new Employee(3, "Tai", "Phat", 30);

           // In thông tin 3 đối tượng ra console
          System.out.println(employee1);
          System.out.println(employee2);
          System.out.println(employee3);

           // Khởi tạo một ArrayList mới và thêm các đối tượng nhân viên vào
          List<Employee> employeeList = new ArrayList<>();
          employeeList.add(employee1);
          employeeList.add(employee2);
          employeeList.add(employee3);

          // Trả về ArrayList
          return employeeList;
     }
}
