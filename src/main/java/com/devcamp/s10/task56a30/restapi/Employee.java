package com.devcamp.s10.task56a30.restapi;


public class Employee {
     private int id;
     private String firstName;
     private String lastName;
     private int salary;


     public Employee(int id, String firstName, String lastName, int salary) {
          this.id = id;
          this.firstName = firstName;
          this.lastName = lastName;
          this.salary = salary;
     }


     public int getId() {
          return id;
     }


     public String getFirstName() {
          return firstName;
     }


     public String getLastName() {
          return lastName;
     }
     
     public String getName(){
          return this.firstName + this.lastName;
     }


     public int getSalary() {
          return salary;
     }


     public void setSalary(int salary) {
          this.salary = salary;
     }

     public int getAnnualSalary(int percent){
          return salary * 12;
     }

     public int raiseSalary(int percent){
          return salary * percent / 100;
     }


     @Override
     public String toString() {
          return "Employee [id=" + this.id + ", Name= " + this.firstName + " " + this.lastName + ", salary=" + this.salary
                    + "]";
     }

     

     

     
}
